# Repositório da Turma Java Orientação à Objetos N1803 - Senai Informatica 1.32
## Bem Vindo
Aqui são disponibilizados os projetos Java, as apresentações, exercícios e respostas.
## Como obter
Para obter uma cópia deste conteúdo basta utilizar o comando:

```
git clone git@gitlab.com:JSWilProf/javaoo2018.git
```

Também é possível fazer o download através do link
[JavaOO2018](https://gitlab.com/JSWilProf/javaoo2018.git)

# Ementa

## Módulo Principal (40h)

- Classes e Objetos
- Atributos e métodos
- Encapsulamento
- Tipos de Classes
- Relacionamento entre Objetos
- Definição e utilização de interfaces
- Interfaces Funcionais
- Médotos defaul e estáticos da interface
- Expressões Lambda

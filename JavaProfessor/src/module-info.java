/**
 * @author prof
 *
 */
module javaprofessor {
	requires java.desktop;
	requires java.sql;
	requires static lombok;
	requires static org.mapstruct.processor;
	
	opens br.senai.sp.informatica.thread.mario;
}
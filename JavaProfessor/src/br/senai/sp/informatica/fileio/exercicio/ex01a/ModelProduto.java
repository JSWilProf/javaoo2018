package br.senai.sp.informatica.fileio.exercicio.ex01a;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.senai.sp.informatica.oo.gui.lib.CurrencyFormat;
import br.senai.sp.informatica.oo.gui.lib.DataAccessProvider;

@SuppressWarnings("serial")
public class ModelProduto extends AbstractTableModel implements PropertyChangeListener {
	private static CurrencyFormat formatador = new CurrencyFormat();
	private DataAccessProvider<Produto, Integer> provider;
	private String[] titulo = { "Cod.", "Nome" , "Valor", "Qtd." };
	
	private List<Produto> lista;
	
	public ModelProduto(DataAccessProvider<Produto, Integer> provider) {
		this.provider = provider;
		provider.addListener(this);
		
		try {
			lista = provider.consultar();
		} catch (Exception e) {
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evento) {
		String tipo = evento.getPropertyName();
		Produto novo = (Produto)evento.getNewValue();
		
		if(tipo.equals(DataAccessProvider.EVENTO_INCLUIR)) {
				lista.add(novo);
				fireTableRowsInserted(lista.size()-1, lista.size()-1);
		} else if(tipo.equals(DataAccessProvider.EVENTO_ATUALIZAR)) {
			int linha = -1;
			
			for (int posicao = 0; posicao < lista.size(); posicao++) {
				Produto produto = lista.get(posicao);
				if(produto.getIdproduto() == novo.getIdproduto()) {
					linha = posicao;
					break;
				}
			}	
			
			if(linha > -1) {
				lista.set(linha, novo);
				fireTableRowsUpdated(linha, linha);
			}
		} else if(tipo.equals(DataAccessProvider.EVENTO_EXCLUIR)) {
			int linha = -1;
			
			for (int posicao = 0; posicao < lista.size(); posicao++) {
				Produto produto = lista.get(posicao);
				if(produto.getIdproduto() == novo.getIdproduto()) {
					linha = posicao;
					break;
				}
			}	
			
			if(linha > -1) {
				lista.remove(linha);
				fireTableRowsDeleted(linha, linha);
			}
		}	
	}

	@Override
	public int getRowCount() {
		return lista != null ? lista.size() : 0;
	}
	
	@Override
	public int getColumnCount() {
		return titulo.length;
	}

	@Override
	public String getColumnName(int col) {
		return titulo[col];
	}

	@Override
	public Object getValueAt(int lin, int col) {
		Object valor = null;
		Produto prod = lista.get(lin);
		
		switch (col) {
		case 0:
			valor = prod.getCodigo();
			break;
		case 1:
			valor = prod.getNome();
			break;
		case 2:
			valor = formatador.format(prod.getValor());
			break;
		case 3:
			valor = prod.getQuantidade();
			break;
		}
		
		return valor;
	}

	@Override
	public void setValueAt(Object valor, int lin, int col) {
		Produto prod = lista.get(lin);
		
		try {
			switch (col) {
			case 0:
				prod.setCodigo(Integer.parseInt((String)valor));
				break;
			case 1:
				prod.setNome((String)valor);
				break;
			case 2:
				prod.setValor(formatador.parse((String)valor));
				break;
			case 3:
				prod.setQuantidade(Integer.parseInt((String)valor));
				break;
			}
			
			provider.atualizar(prod);
		} catch (Exception ex) {
		}
	}

	@Override
	public boolean isCellEditable(int lin, int col) {
		return true;
	}
}

package br.senai.sp.informatica.fileio.exercicio.ex01a;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FileDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import br.senai.sp.informatica.oo.gui.lib.IntegerTextField;
import br.senai.sp.informatica.oo.gui.lib.NumberTextField;
import br.senai.sp.informatica.oo.gui.lib.SwUtil;
import br.senai.sp.informatica.oo.gui.lib.TableVerificaInteiro;
import br.senai.sp.informatica.oo.gui.lib.TableVerificaValor;
import br.senai.sp.informatica.oo.gui.lib.VerificaCellEditor;

@SuppressWarnings("serial")
public class CadProdutos extends JFrame implements ActionListener {
	private JPanel contentPane;
	private JLabel lblCod;
	private IntegerTextField tfCod;
	private JLabel lblNome;
	private JTextField tfNome;
	private JLabel lblDescrio;
	private JTextField tfDesc;
	private JLabel lblValor;
	private NumberTextField tfValor;
	private JLabel lblQuantidade;
	private IntegerTextField tfQtd;
	private JScrollPane scrollPane;
	private JTable table;
	private JButton btnInserir;
	private JButton btnImportar;
	private JButton btnSair;
	private JButton btnExportar;
	
	private ProdutoDao cadastro;
	private ModelProduto model;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadProdutos frame = new CadProdutos();
					frame.cadastro = new ProdutoDao();
					frame.model = new ModelProduto(frame.cadastro);
					frame.table.setModel(frame.model);
					frame.configuraTabela(frame.table);
					frame.setVisible(true);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
	}

	public CadProdutos() {
		setMinimumSize(new Dimension(450, 300));
		setTitle("Cadastro de Produtos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		lblCod = new JLabel("Cod.");
		
		tfCod = new IntegerTextField();
		tfCod.setColumns(10);
		
		lblNome = new JLabel("Nome");
		
		tfNome = new JTextField();
		tfNome.setColumns(10);
		
		lblDescrio = new JLabel("Descri\u00E7\u00E3o");
		
		tfDesc = new JTextField();
		tfDesc.setColumns(10);
		
		lblValor = new JLabel("Valor");
		
		tfValor = new NumberTextField();
		tfValor.setColumns(10);
		
		lblQuantidade = new JLabel("Quantidade");
		
		tfQtd = new IntegerTextField();
		tfQtd.setColumns(10);
		
		scrollPane = new JScrollPane();
		
		btnInserir = new JButton("Inserir");
		btnInserir.addActionListener(this);
		
		btnImportar = new JButton("Importar");
		btnImportar.addActionListener(this);
		
		btnSair = new JButton("Sair");
		btnSair.addActionListener(this);
		
		btnExportar = new JButton("Exportar");
		btnExportar.addActionListener(this);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblCod)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tfCod, GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
							.addGap(18)
							.addComponent(lblNome)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tfNome, GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblDescrio)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tfDesc, GroupLayout.DEFAULT_SIZE, 360, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblValor)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tfValor, GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
							.addGap(18)
							.addComponent(lblQuantidade)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(tfQtd, GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)))
					.addContainerGap())
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addGap(20)
					.addComponent(btnInserir, GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE)
					.addGap(18)
					.addComponent(btnImportar, GroupLayout.PREFERRED_SIZE, 81, Short.MAX_VALUE)
					.addGap(18)
					.addComponent(btnExportar)
					.addGap(18)
					.addComponent(btnSair, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(9))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCod)
						.addComponent(tfCod, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNome)
						.addComponent(tfNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDescrio)
						.addComponent(tfDesc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblValor)
						.addComponent(tfValor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblQuantidade)
						.addComponent(tfQtd, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
					.addGap(10)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnInserir)
						.addComponent(btnImportar)
						.addComponent(btnExportar)
						.addComponent(btnSair))
					.addContainerGap())
		);
		
		table = new JTable();
		table.setAutoCreateRowSorter(true);
		scrollPane.setViewportView(table);
		contentPane.setLayout(gl_contentPane);
	}
	
	public void actionPerformed(ActionEvent ev) {
		Object cmd = ev.getSource();
		
		try {
			if(cmd.equals(btnInserir)) {
				Produto prod = new Produto();
				prod.setCodigo(tfCod.getValue());
				prod.setNome(tfNome.getText());
				prod.setDescricao(tfDesc.getText());
				prod.setValor(tfValor.getValue());
				prod.setQuantidade(tfQtd.getValue());
				
				cadastro.incluir(prod);
				
				SwUtil.limpa(this);
				tfCod.requestFocus();
			} else if(cmd.equals(btnImportar)) {
				importarProdutos(cadastro);
			} else if(cmd.equals(btnExportar)) {
				exportarProdutos(cadastro.consultar());
			} else {
				cadastro.fecharConexao();
				System.exit(0);
			}
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(this, ex.getMessage());
		}
	}

	private void configuraTabela(JTable table) {
		TableColumnModel colModel = table.getColumnModel();
		
		DefaultTableCellRenderer cellRenderer0 = new DefaultTableCellRenderer();
		cellRenderer0.setHorizontalAlignment(SwingConstants.RIGHT);
		TableColumn col0 = colModel.getColumn(0);
		col0.setCellEditor(new VerificaCellEditor(new TableVerificaInteiro()));
		col0.setCellRenderer(cellRenderer0);
		col0.setMaxWidth(40);
		
		TableColumn col1 = colModel.getColumn(1);
		col1.setMinWidth(250);
		
		DefaultTableCellRenderer cellRenderer2 = new DefaultTableCellRenderer();
		cellRenderer2.setHorizontalAlignment(SwingConstants.RIGHT);
		TableColumn col2 = colModel.getColumn(2);		
		col2.setCellEditor(new VerificaCellEditor(new TableVerificaValor()));
		col2.setCellRenderer(cellRenderer2);
		col2.setMaxWidth(100);

		DefaultTableCellRenderer cellRenderer3 = new DefaultTableCellRenderer();
		cellRenderer3.setHorizontalAlignment(SwingConstants.RIGHT);
		TableColumn col3 = colModel.getColumn(3);		
		col3.setCellEditor(new VerificaCellEditor(new TableVerificaInteiro()));
		col3.setCellRenderer(cellRenderer3);
		col2.setMaxWidth(70);
	}
	
	private void exportarProdutos(List<Produto> lista) {
		String nomeDoArquivo = obtemONomeDoArguivo(FileDialog.SAVE);
		if(nomeDoArquivo != null) {
			try (PrintWriter gravador = new PrintWriter(nomeDoArquivo)) {
				for (Produto produto : lista) {
					gravador.println(produto);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				JOptionPane.showMessageDialog(this, "Falha ao Exportar os Produtos");
			}
		}
	}
	
	private void importarProdutos(ProdutoDao dao) {
		String nomeDoArquivo = obtemONomeDoArguivo(FileDialog.LOAD);
		if(nomeDoArquivo != null) {
			try (BufferedReader leitor = new BufferedReader(new FileReader(nomeDoArquivo))) {
				String linha = leitor.readLine();
				while (linha != null) {
					dao.incluir(Produto.toProduto(linha));
					
					linha = leitor.readLine();
				}
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(this, "Falha ao Importar os Produtos");
			}
		}
	}

	private String obtemONomeDoArguivo(int modo) {
		FileDialog fd = new FileDialog(this, "Selecione o nome do arquivo que contém a lista de produtos", modo);
		fd.setLocationRelativeTo(this);
		fd.setFile("*.dat");
		fd.setVisible(true);
		
		String nome = fd.getFile();
		
		if(nome != null) {
			return fd.getDirectory() + nome;
		} else {
			return null;
		}
	}
}

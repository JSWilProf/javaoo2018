package br.senai.sp.informatica.fileio.exercicio.ex01a;

import java.io.Serializable;

import br.senai.sp.informatica.oo.gui.lib.CurrencyFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Produto implements Serializable {
	private static final long serialVersionUID = -2336739401904823604L;
	private static final CurrencyFormat conversor = new CurrencyFormat();
	
	private Integer idproduto;
	private int codigo;
	private String nome;
	private String descricao;
	private double valor;
	private int quantidade;

	@Override
	public String toString() {
		return String.format("%d;%s;%s;%,.2f;%d", codigo, nome, descricao, valor, quantidade);
	}
	
	public static Produto toProduto(String dados) throws Exception {
		try {
			String atributo[] = dados.split(";");
			return Produto.builder()
						.codigo(Integer.parseInt(atributo[0]))
						.nome(atributo[1])
						.descricao(atributo[2])
						.valor(conversor.parse(atributo[3]))
						.quantidade(Integer.parseInt(atributo[4]))
						.build();
		} catch (Exception ex) {
			throw new Exception("Dados inválidos");
		}
	}
}

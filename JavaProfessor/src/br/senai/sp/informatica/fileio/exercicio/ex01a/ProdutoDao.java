package br.senai.sp.informatica.fileio.exercicio.ex01a;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.oo.gui.lib.DataAccessException;
import br.senai.sp.informatica.oo.gui.lib.DataAccessProvider;

/* Data Access Object
// um padrão de desenvolvimento de projetos utilizado
// para concentrar o acesso a um determinado mecanismo
// que forneça dados para uma aplicação
*/
public class ProdutoDao extends DataAccessProvider<Produto, Integer> {
	private Connection con;
	private PreparedStatement incluir;
	private PreparedStatement atualizar;
	private PreparedStatement consultar;
	private PreparedStatement obterId;
	private PreparedStatement remover;
	
	public ProdutoDao() throws DataAccessException {
		try {
			// Registrar o Driver JDBC para MySql 5.x
			Class.forName("com.mysql.jdbc.Driver");
			
//			// Registrar o Driver JDBC para MySql 8.x
//			Class.forName("com.mysql.cj.jdbc.Driver");
	
			// Estabelecer a conexão com o Bando de Dados para MySql 5.x
			con = DriverManager.getConnection("jdbc:mysql://localhost:3307/javaoo1803", 
					"root", "root132");

			// Estabelecer a conexão com o Bando de Dados para MySql 5.x
//			con = DriverManager.getConnection("jdbc:mysql://localhost:3307/javaoo1803" +
//					"?useTimezone=true&serverTimezone=UTC&useSSL=false", 
//					"root", "root132");
			
			// Preparar os SQLs para a utilização posterior
			incluir = con.prepareStatement(
					"insert into produto (codigo, nome, descricao, valor, quantidade) values (?,?,?,?,?)");
			
			atualizar = con.prepareStatement(
					"update produto set codigo=?, nome=?, descricao=?, valor=?, quantidade=? where idproduto=?");
			
			consultar = con.prepareStatement("select * from produto");

			obterId = con.prepareStatement("select last_insert_id()");
			
			remover = con.prepareStatement("delete produto where idproduto=?");
		} catch (ClassNotFoundException ex) {
			throw new DataAccessException("O Driver JDBC não foi encontrato");
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new DataAccessException("Houve problema na conexão com o Banco de Dados");
		}
	}
	
	// construção das rotinas que farão a gestão dos dados
	public void incluir(Produto obj) throws DataAccessException {
		try {
			incluir.setInt(1,  obj.getCodigo());
			incluir.setString(2, obj.getNome());
			incluir.setString(3, obj.getDescricao());
			incluir.setDouble(4, obj.getValor());
			incluir.setInt(5, obj.getQuantidade());
			incluir.execute();
			
			ResultSet result = obterId.executeQuery();
			if(result.next()) obj.setIdproduto(result.getInt(1));
			
			super.incluir(obj);
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new DataAccessException("Falha ao incluir um Fornecedor");
		}
	}
	
	public void atualizar(Produto obj) throws DataAccessException {
		try {
			atualizar.setInt(1, obj.getCodigo());
			atualizar.setString(2, obj.getNome());
			atualizar.setString(3, obj.getDescricao());
			atualizar.setDouble(4, obj.getValor());
			atualizar.setInt(5, obj.getQuantidade());
			atualizar.setInt(6, obj.getIdproduto());
			atualizar.execute();
			
			super.atualizar(obj);
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new DataAccessException("Falha ao atualizar um Fornecedor");
		}
	}
	
	public List<Produto> consultar() throws DataAccessException {
		try {
			List<Produto> lista = new ArrayList<>();
			
			ResultSet resultado = consultar.executeQuery();
			while(resultado.next()) {
				Produto obj = new Produto();
				obj.setIdproduto(resultado.getInt("idproduto"));
				obj.setCodigo(resultado.getInt("codigo"));
				obj.setNome(resultado.getString("nome"));
				obj.setDescricao(resultado.getString("descricao"));
				obj.setValor(resultado.getDouble("valor"));
				obj.setQuantidade(resultado.getInt("quantidade"));
				
				lista.add(obj);
			}
			
			return lista;
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new DataAccessException("Houve falha ao listar os Fornecedores");
		}
	}
	
	public void remover(int id) throws DataAccessException {
		try {
			remover.setInt(1, id);
			remover.execute();
			
			super.remover(id);
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new DataAccessException("Falha ao remover o Fornecedor");
		}
	}
	
	public void fecharConexao() {
		try {
			if(con != null) {
				con.close();
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}








package br.senai.sp.informatica.thread.ball;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
class Bounce extends JFrame implements ActionListener {
	private JButton btStart;
	private JButton btClose;
	private JPanel canvas = new JPanel();

	public Bounce() {
		setTitle("Bounce");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Container contentPane = getContentPane();
		canvas.setPreferredSize(new Dimension(450, 200));
		contentPane.add(canvas, "Center");
		
		JPanel temp = new JPanel();
		
		btStart = new JButton("Iniciar");
		btStart.addActionListener(this);
		temp.add(btStart);
		
		btClose = new JButton("Fechar");
		btClose.addActionListener(this);
		temp.add(btClose);

		contentPane.add(temp, "South");
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		Object botao = ev.getSource();
		
		if(botao.equals(btStart)) {
			SimpleBall b = new SimpleBall(canvas);
			Thread t = new Thread(b);
			t.start();
		} else {
			canvas.setVisible(false);
			System.exit(0);
		}		
	}
	
	class SimpleBall extends Ball 
	implements Runnable {

		public SimpleBall(JPanel painel) {
			super(painel);
		}

		public void run() {
			try {
				draw();
				for(;;) {
					move();
					Thread.sleep(5);
				}
			} catch (InterruptedException e) {
			}
		}
	}

	public static void main(String[] args) {
		new Bounce();
	}
}

package br.senai.sp.informatica.oo.exemplos.agregacao;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Time {
	private String nome;
	private String tecnico;
	@Singular
	private List<Atleta> atletas;

	@Override
	public String toString() {
		return "nome: " + nome + " tecnico: " + tecnico + " atletas: " + atletas;
	}

}

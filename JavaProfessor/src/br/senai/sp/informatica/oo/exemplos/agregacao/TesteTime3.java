package br.senai.sp.informatica.oo.exemplos.agregacao;

import java.util.ArrayList;
import java.util.List;

import lombok.var;

public class TesteTime3 {
	public static void main(String[] args) {
		// Catálogo de todos os Times
		var lista = new ArrayList<Time>();

		// Construindo um Time
		Time time = new Time();
		time.setNome("Senai");
		time.setTecnico("Bona");

		Atleta atleta1 = new Atleta();
		atleta1.setNome("Jão");
		atleta1.setIdade(22);
		atleta1.setPosicao("Centro - esquerda");

		Atleta atleta2 = new Atleta();
		atleta2.setNome("Zé");
		atleta2.setIdade(24);
		atleta2.setPosicao("meia - direita");
						
		// Criando a lista de atletas para UM time
		List<Atleta> atletas = new ArrayList<>();
		atletas.add(atleta1);
		atletas.add(atleta2);
				
		// Atribuindo a lista de atletas para o time
		time.setAtletas(atletas);
		
		// Cadastrando o Time c/ atletas no Catálogo de Times
		lista.add(time);

		System.out.println(lista);
	}
}

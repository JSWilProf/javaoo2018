package br.senai.sp.informatica.oo.exemplos.agregacao;

import java.util.ArrayList;

import lombok.var;

public class TesteTime {
	public static void main(String[] args) {
		// Catálogo de todos os Times
		var lista = new ArrayList<Time>();

		// Construindo um Time
		Time a = Time.builder()
					.nome("Senai")
					.tecnico("Bona")
					.atleta(Atleta.builder()
						.nome("Jão")
						.idade(22)
						.posicao("Centro - esquerda")
						.build())
					.atleta(Atleta.builder()
						.nome("Zé")
						.idade(24)
						.posicao("meia - direita")
						.build())
					.build();
		
		// Cadastrando o Time c/ atletas no Catálogo de Times
		lista.add(a);

		System.out.println(lista);
	}
}

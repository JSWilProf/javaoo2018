package br.senai.sp.informatica.oo.exemplos.agregacao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Atleta {
	private String nome;
	private int idade;
	private String posicao;
	
	@Override
	public String toString() {
		return "nome: " + nome + " idade: " + idade + " posicao: " + posicao;
	}

}

package br.senai.sp.informatica.oo.exemplos.objetos.agregacao.itemDePedido;

import java.awt.EventQueue;

import br.senai.sp.informatica.oo.exemplos.objetos.agregacao.itemDePedido.view.CadastroDeItens;

public class Main {
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroDeItens frame = new CadastroDeItens();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}

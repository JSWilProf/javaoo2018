package br.senai.sp.informatica.oo.exemplos.agregacao;

import java.util.ArrayList;

import lombok.var;

public class TesteTime2 {
	public static void main(String[] args) {
		// Catálogo de todos os Times
		var lista = new ArrayList<Time>();

		// Construindo um Time
		Time a = Time.builder()
					.nome("Senai")
					.tecnico("Bona")
					.build();

		// Criando a lista de atletas para UM time
		var atletas = new ArrayList<Atleta>();
		
		// Adicionando um atleta à lista
		atletas.add(Atleta.builder()
						.nome("Jão")
						.idade(22)
						.posicao("Centro - esquerda")
						.build());

		// Adicionando mais um atleta à lista
		atletas.add(Atleta.builder()
				.nome("Zé")
				.idade(24)
				.posicao("meia - direita")
				.build());
		
		// Atribuindo a lista de atletas para o time
		a.setAtletas(atletas);
		
		// Cadastrando o Time c/ atletas no Catálogo de Times
		lista.add(a);

		System.out.println(lista);
	}
}

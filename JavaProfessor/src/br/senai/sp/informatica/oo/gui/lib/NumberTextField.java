package br.senai.sp.informatica.oo.gui.lib;

import java.text.ParseException;

@SuppressWarnings("serial")
public class NumberTextField extends OptionalTextField {
	private CurrencyFormat numberFormat = new CurrencyFormat();
	
	@Override
	public Double getValue() {
		try {
			return numberFormat.parse(getText());
		} catch (ParseException ex) {
			return (Double)super.getValue();
		}
	}
}

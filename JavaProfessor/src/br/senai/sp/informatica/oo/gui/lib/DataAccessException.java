package br.senai.sp.informatica.oo.gui.lib;

@SuppressWarnings("serial")
public class DataAccessException extends Exception {
	public DataAccessException(String message) {
		super(message);
	}
}

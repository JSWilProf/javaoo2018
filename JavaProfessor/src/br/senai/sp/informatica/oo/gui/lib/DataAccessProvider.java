package br.senai.sp.informatica.oo.gui.lib;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;

public abstract class DataAccessProvider<Tipo, Chave>  {
	private PropertyChangeSupport listeners = new PropertyChangeSupport(this);
	
	public static String EVENTO_INCLUIR = "incluir";
	public static String EVENTO_ATUALIZAR = "atualizar";
	public static String EVENTO_EXCLUIR = "excluir";
	

    public void addListener(PropertyChangeListener listener) {
    	listeners.addPropertyChangeListener(listener);
    }

    public void removeListener(PropertyChangeListener listener) {
    	listeners.removePropertyChangeListener(listener);
    }

    public void incluir(Tipo obj) throws DataAccessException {
    	listeners.firePropertyChange(EVENTO_INCLUIR , null, obj);
    }

    public void atualizar(Tipo obj) throws DataAccessException {
    	listeners.firePropertyChange(EVENTO_ATUALIZAR, null, obj);
    }

    public void remover(Chave id) throws DataAccessException {
    	listeners.firePropertyChange(EVENTO_EXCLUIR , null, id);
    }

    public abstract List<Tipo> consultar() throws DataAccessException;
}

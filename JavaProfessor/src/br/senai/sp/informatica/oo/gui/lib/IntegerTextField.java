package br.senai.sp.informatica.oo.gui.lib;

import java.text.ParseException;

@SuppressWarnings("serial")
public class IntegerTextField extends OptionalTextField {
	private IntegerFormat format = new IntegerFormat();
	@Override
	public Integer getValue() {
		try {
			return format.parse(getText());
		} catch (ParseException ex) {
			return (Integer)super.getValue();
		}
	}
}

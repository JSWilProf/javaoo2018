package br.senai.sp.informatica.oo.gui.lib;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.ParsePosition;

public class IntegerFormat extends DecimalFormat {
	private static final long serialVersionUID = -554614047141234496L;
	
	public IntegerFormat() {
		super("#,##0;-#,##0");
		super.setMaximumFractionDigits(0);
		super.setMinimumFractionDigits(0);
	}

	@Override
	public Integer parse(String value) throws ParseException {
	    final ParsePosition pos = new ParsePosition(0);
	    final Number parsedNumber = super.parse(value, pos);
	    
	    if (pos.getErrorIndex() >= 0 || pos.getIndex() != value.length() || parsedNumber == null) {
	        throw new ParseException("Número inválido", pos.getErrorIndex());
	    }
	    return parsedNumber.intValue();
	}
}
